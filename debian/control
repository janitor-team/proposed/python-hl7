Source: python-hl7
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: John Paulett <john@paulett.org>,
           Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3,
               python3-setuptools,
               python3-six,
               python3-mock,
               debhelper
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/med-team/python-hl7
Vcs-Git: https://salsa.debian.org/med-team/python-hl7.git
Homepage: https://github.com/johnpaulett/python-hl7/
Rules-Requires-Root: no

Package: python3-hl7
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-pkg-resources
Recommends: python3-six,
            python3-mock
Provides: ${python:Provides},
          python-hl7
Description: Python3 library for parsing HL7 messages
 HL7 is a communication protocol and message format for health care data.
 It is the de-facto standard for transmitting data between clinical
 information systems and between clinical devices. The version 2.x series,
 which is often is a pipe delimited format is currently the most widely
 accepted version of HL7 (version 3.0 is an XML-based format).
 python-hl7 currently only parses HL7 version 2.x messages into an easy
 to access, list-based, data structure.
 .
 This package contains the Python3 API.
